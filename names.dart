// Write a basic program that stores and then prints
// the following data: Your name, favorite app, and city

import 'dart:io';

void main(List<String> args) {
  var myName = "Melvine Tebogo Lichabe";
  var myFavoriteApp = "Facebook";
  var myCity = "Klerksdorp";

  print("My name is $myName," +
      " " +
      "my favorite app is $myFavoriteApp" +
      " " +
      "and my city is $myCity");

  print("____________________________________");

  print("Hello MTN, life is all about new things...details needed here");

  print("___________________________________");

  stdout.writeln("Please enter your name: ");
  var name = stdin.readLineSync().toString();

  stdout.writeln("Please enter your favourite app: ");
  var favouriteApp = stdin.readLineSync().toString();

  stdout.writeln("Please enter your city: ");
  var city = stdin.readLineSync().toString();

  print("Your name is $name " +
      ", your favorite App is $favouriteApp " +
      "and the name of your city is $city.");
}
