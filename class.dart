void main(List<String> args) {
  var appInfo_1 =
      Apps('Khula', 'agriculture sector', 'Karidas Tshintsholo', 2018);
  var appInfo_2 = Apps('FNB Banking App', 'consumer sector', 'FNB', 2012);
  var appInfo_3 = Apps('SnapScan', 'consumer sector', 'Gerrit Greef', 2013);
  var appInfo_4 = Apps('WumDrop', 'Enterprise sector', 'Simon Hartley', 2015);
  var appInfo_5 = Apps('Domestly', 'consumer sector', 'Berno Potgieter', 2016);
  var appInfo_6 = Apps('Shyft', 'finance sector', 'Breth Patrontasch', 2017);
  var appInfo_7 =
      Apps('Naked Insurance', 'finance sector', 'Sumarie Greybe', 2019);
  var appInfo_8 =
      Apps('EasyEquity', 'investment sector', 'Charles Savage', 2020);
  var appInfo_9 = Apps('Ambani', 'education sector', 'Mukundi Lambani', 2018);
  var appInfo_10 = Apps('Live inspect', 'consumer sector', 'Lightstone', 2014);

  var list = [];
  list.add(appInfo_1.toJson());
  list.add(appInfo_2.toJson());
  list.add(appInfo_3.toJson());
  list.add(appInfo_4.toJson());
  list.add(appInfo_5.toJson());
  list.add(appInfo_6.toJson());
  list.add(appInfo_7.toJson());
  list.add(appInfo_8.toJson());
  list.add(appInfo_9.toJson());
  list.add(appInfo_10.toJson());

  for (var _list in list) {
    print(_list);
  }
}

class Apps {
  var appName;
  var sector;
  var developer;
  var year;

  Apps(String appName, String sector, String developer, int year) {
    this.appName = appName;
    this.sector = sector;
    this.developer = developer;
    this.year = year;
  }

  Map<String, dynamic> toJson() {
    return {
      'App name': appName.toUpperCase(),
      'Sector': sector,
      'Developer': developer,
      'Year': year
    };
  }
}
